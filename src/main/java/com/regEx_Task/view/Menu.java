package com.regEx_Task.view;

import com.regEx_Task.model.Action;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.Set;

public class Menu {
  Locale locale;
  ResourceBundle bundle;
  Map<Integer, String> menu;
  Map<Integer, String> methods;
  Action action = new Action();
  Scanner sc = new Scanner(System.in);

  private void setMenu(){
    menu = new LinkedHashMap<>();
    menu.put(1, bundle.getString("1"));
    menu.put(2, bundle.getString("2"));
    menu.put(3, bundle.getString("3"));
    menu.put(4, bundle.getString("4"));
    menu.put(5, bundle.getString("5"));
    menu.put(6, bundle.getString("6"));
    menu.put(7, bundle.getString("7"));
    menu.put(8, bundle.getString("8"));
    menu.put(9, bundle.getString("9"));
    menu.put(10, bundle.getString("10"));
    menu.put(11, bundle.getString("11"));
    menu.put(12, bundle.getString("12"));
    menu.put(13, bundle.getString("13"));
    menu.put(14, bundle.getString("14"));
    menu.put(15, bundle.getString("15"));
    menu.put(16, bundle.getString("16"));
  }

  private void show(){
    for (String value : menu.values()) {
      System.out.println(value);
    }
  }

  public int languageChoosing(){
    System.out.println("Please, choose the language");
    int i = sc.nextInt();
    int choice = 0;
    switch (i){
      case 1:
        englishMenu();
        choice = 1;
        break;
      case 2:
        ukrainianMenu();
        choice = 2;
        break;
        default:
          System.out.println("Choose correct number");
        break;
    }
    return choice;
  }

  private Map<Integer, String> methodsMap() throws FileNotFoundException {
    methods = new LinkedHashMap<>();
    methods.put(1, action.findNumberOrSentencesWithDuplicatedWords_1_N());
    return methods;
  }

  public void showMethodsResults(int choice) throws FileNotFoundException {
    Map<Integer, String> methods = methodsMap();
    methods.get(choice);
  }

  private void englishMenu(){
  locale = new Locale("en");
  bundle = ResourceBundle.getBundle("Bundle", locale);
  setMenu();
  show();
  }

  private void ukrainianMenu(){
    locale = new Locale("uk");
    bundle = ResourceBundle.getBundle("Bundle", locale);
    setMenu();
    show();
  }

}
