package com.regEx_Task.model;

import com.regEx_Task.model.component.Punctuation;
import com.regEx_Task.model.component.Sentence;
import com.regEx_Task.model.component.Word;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BookParser {

  private File book = new File("E:\\Epam\\Task files\\Task9_RegEx\\Book.txt");
  final static String WORD_REGEX = "\\s*\\w+[^\\s!?,.:;()'-]";
  final static String SENTENCE_REGEX = "[^.?!]+[.?!]";
  final static String PUNCTUATION_REGEX = "\\s*[^\\s\\!\\?\\,\\.\\:\\;]";

  public String getText() throws FileNotFoundException {
    Scanner sc = new Scanner(book);
    String text = "";
    while (sc.hasNextLine()){
      String nextLine = sc.nextLine();
      String line1 = nextLine.replaceAll("\\t", " ");
      String line = line1.replaceAll(" {2,}", " ");
      text = text.concat(line + "\n");
    }
    return text;
  }

  public List<Word> getWords() throws FileNotFoundException {
    String text = getText();
    String[] wordsArr = text.split("[,.!?\\s]+");
    List<Word> words = new ArrayList<>();
      for (String wordStr : wordsArr) {
        String wordFinal = wordStr.replaceAll("[,\\s]", "");
        Word word = new Word(wordFinal);
        words.add(word);
      }
    return words;
  }
  public List<Word> getWords2() throws FileNotFoundException {
    String text = getText();
    Pattern p = Pattern.compile(WORD_REGEX);
    Matcher m = p.matcher(text);
    List<Word> words = new ArrayList<>();
    while (m.find()){
      String word = text.substring(m.start(), m.end());
      String trim = word.trim();
      words.add(new Word(trim));
    }
    return words;
  }
  public List<Punctuation> getMarks() throws FileNotFoundException {
    String text = getText();
    String[] sentences = text.split("[.]|[?]|[!]");
    List<Punctuation> marks = new LinkedList<>();
    for (String sentence : sentences) {
      String[] chars = sentence.split("\\w+");
      for (String aChar : chars) {
        Punctuation punctuation = new Punctuation(aChar);
        marks.add(punctuation);
      }
    }
    return marks;
  }
  public List<Sentence> getSentences() throws FileNotFoundException {
    String text = getText();
    String[] sentences = text.split("[.]|[?]|[!]");
    List<Sentence> sentenceCls = new LinkedList<>();
    for (String sentence : sentences) {
      Sentence sentenceCl = new Sentence(sentence);
      sentenceCls.add(sentenceCl);
    }
    return sentenceCls;
  }

  public List<Sentence> getSentences2() throws FileNotFoundException {
    String text = getText();
    Pattern p = Pattern.compile(SENTENCE_REGEX);
    Matcher m = p.matcher(text);
    List<Sentence> sentences = new LinkedList<>();
    while (m.find()){
      String substring = text.substring(m.start(), m.end());
      sentences.add(new Sentence(substring));
    }
    return sentences;
  }
}
