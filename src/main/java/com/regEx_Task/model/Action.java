package com.regEx_Task.model;

import static java.util.Map.Entry.comparingByValue;

import com.regEx_Task.model.component.Sentence;
import com.regEx_Task.model.component.Word;
import java.io.FileNotFoundException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Action {

  private BookParser bookParser = new BookParser();

  public void printOrderedSentences_2() throws FileNotFoundException {

    List<Sentence> sentences = bookParser.getSentences();
    Map<Sentence, Integer> sentencesMap = new LinkedHashMap<>();
    for (Sentence sentence : sentences) {
      String[] words = sentence.getSentence().split("\\W+");
      sentencesMap.put(sentence, words.length);
    }
    System.out.println(sentencesMap);
    LinkedHashMap<Sentence, Integer> orderedSentencesByWordsNumber = sentencesMap.entrySet()
        .stream()
        .sorted(comparingByValue(Comparator.reverseOrder()))
        .collect(Collectors.toMap(
            Entry::getKey,
            Entry::getValue,
            (x, y) -> {
              throw new AssertionError();
            },
            LinkedHashMap::new
        ));
    Set<Entry<Sentence, Integer>> entries = orderedSentencesByWordsNumber.entrySet();
    Iterator<Entry<Sentence, Integer>> iterator = entries.iterator();
    while (iterator.hasNext()) {
      Entry<Sentence, Integer> next = iterator.next();
      System.out.println(next.getValue() + " " + next.getKey());
    }
  }

  public void findUniqueWordFromFirstSentence_3() throws FileNotFoundException {
    List<Sentence> sentences = bookParser.getSentences();
    Sentence firstSentence = sentences.get(0);
    String[] wordsOfFirstSentence = firstSentence.getSentence().split("\\W+");
    List<Word> allWordsFromText = bookParser.getWords();
    List<String> allWordsNameFromText = new ArrayList<>();
    for (String wordName : wordsOfFirstSentence) {
      int counter = 0;
      for (Word word : allWordsFromText) {
        if (wordName.equals(word.getWord())) {
          counter++;
        }
      }
      if (counter == 1) {
        System.out.println(wordName);
      }
    }
    String text = bookParser.getText();
  }

  public String findNumberOrSentencesWithDuplicatedWords_1_N() throws FileNotFoundException {
    int count = 0;
    List<Sentence> sentences = bookParser.getSentences();
    for (Sentence sentence : sentences) {
      String[] words = sentence.getSentence().split("\\W+");
      for (String word : words) {
        int wordsDuplicate = 0;
        for (String wordForComparing : words) {
          if (word.equalsIgnoreCase(wordForComparing)) {
            wordsDuplicate++;
          }
        }
        if (wordsDuplicate > 1) {
          count++;
        }
      }
    }
    return String.valueOf(count);
  }

  public void findSpecificWordsFromQuestionSentence_4(int length) throws FileNotFoundException {
    String text = bookParser.getText();
    Set<String> goalWords = new HashSet<>();
    List<String> list = new ArrayList<>();
    Pattern pattern = Pattern.compile("([.!?]+[\\s])?+[A-Za-z\\s,-]+[?]");
    Matcher matcher = pattern.matcher(text);
    while (matcher.find()) {
      list.add(text.substring(matcher.start(), matcher.end()));
    }
    for (String s : list) {
      String[] words = s.split(" ");
      for (int i = 0; i < words.length; i++) {
        words[i] = words[i].replaceAll("[,?]", "");
      }
      for (String word : words) {
        if (word.length() == length) {
          goalWords.add(word);
        }
      }
    }
    goalWords.forEach(System.out::println);
  }

  public void changeWords_5_N() throws FileNotFoundException {
    List<Sentence> sentences = bookParser.getSentences();
    List<Sentence> newSentences = new LinkedList<>();
    Pattern pattern = Pattern.compile("\\b[aeiou]\\w*");
    Matcher matcher = null;
    String maxLengthWord = "";
    for (int i = 0; i < sentences.size(); i++) {
      String sentence = sentences.get(i).getSentence();
      String[] words = sentence.split("\\s");
      for (int j = 0; j < words.length; j++) {
        words[j] = words[j].replaceAll(",", "");
        matcher = pattern.matcher(words[j]);
        if (matcher.find()) {
          for (int k = 0; k < words.length; k++) {
            if (words[k].length() >= maxLengthWord.length()) {
              maxLengthWord = words[k];
            }
          }
          words[j] = maxLengthWord;
        }
      }
    }
  }

  public void wordsInOrder_6() throws FileNotFoundException {
    List<Word> words1 = bookParser.getWords2();
    List<String> words = new ArrayList<>();
    for (Word word : words1) {
      words.add(word.getWord());
    }
    List<String> collect = words.stream()
        .sorted(String::compareTo)
        .collect(Collectors.toList());
    List<String> orderedWords = new ArrayList<>();
    for (int i = 0; i < collect.size() - 1; i++) {
      if (collect.get(i).charAt(0) != collect.get(i + 1).charAt(0)) {
        orderedWords.add("\n " + collect.get(i + 1));
      } else {
        orderedWords.add(collect.get(i + 1));
      }
    }
    System.out.println(orderedWords);
  }

  public void orderedWordsByVowelLetterPercentage_7_N() throws FileNotFoundException {
//    List<Word> wordEntities = bookParser.getWords2();
//    List<String> words = new ArrayList<>();
//    Set<String> set = new HashSet<>();
//    Pattern p = Pattern.compile("[aeiou]\\w*");
//    Pattern p1 = Pattern.compile("\\b[^aeiou]\\w*");
//    Matcher m;
//    double persentage = 0.0;
//    int wordLength = 0;
//    for (Word wordEntity : wordEntities) {
//      words.add(wordEntity.getWord());
//    }
//    for (int i = 0; i < words.size(); i++) {
//      m = p.matcher(words.get(i));
//      // System.out.println(words.get(i));
//      System.out.println(m.matches());

//    }
    List<Word> wordsEntities = bookParser.getWords2();
    List<String> words = new ArrayList<>();
    for (Word wordsEntity : wordsEntities) {
      words.add(wordsEntity.getWord());
    }
    List<String> stringList = words.stream()
        .sorted(Comparator.comparingDouble(this::getVowelsPercents))
        .collect(Collectors.toList());
    System.out.println(stringList);
  }

  public void orderedWords_8() throws FileNotFoundException {
    List<Word> wordsEntities = bookParser.getWords2();
    List<String> words = new ArrayList<>();
    Pattern p = Pattern.compile("^[aeioyuAEIOYU]");
    Matcher m;
    for (Word wordsEntity : wordsEntities) {
      words.add(wordsEntity.getWord());
    }
    List<String> vowelWords = new ArrayList<>();
    for (int i = 0; i < words.size(); i++) {
      m = p.matcher(words.get(i));
      if (m.find()){
        vowelWords.add(words.get(i));
      }
    }
    List<String> collect = vowelWords.stream()
        .sorted(Comparator.comparingInt(this::getFirstConsonantLetter))
        .collect(Collectors.toList());
    System.out.println(collect);
  }



  public void orderedByNumberOfCertainLetter_9(char st) throws FileNotFoundException {
    char c = 'a';
    Map<String, Integer> wordMap = new HashMap<>();
    List<Word> wordsEntity = bookParser.getWords2();
    List<String> words = new LinkedList<>();
    for (Word word : wordsEntity) {
      words.add(word.getWord());
    }
    List<String> orderedWords = words.stream().sorted().collect(Collectors.toList());
    for (String orderedWord : orderedWords) {
      int count = 0;
      char[] chars = orderedWord.toCharArray();
      for (int i = 0; i < chars.length; i++) {
        if (chars[i] == c){
          count++;
        }
      }
      wordMap.put(orderedWord, count);
    }
    LinkedHashMap<String, Integer> orderedWordsWithCharNumber = wordMap.entrySet().stream().sorted(comparingByValue())
        .collect(
            Collectors.toMap(Entry::getKey, Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));
    System.out.println(orderedWordsWithCharNumber);
  }

public void task_10() throws FileNotFoundException {
  String text = bookParser.getText();
  List<Sentence> sentences = bookParser.getSentences2();
  List<String> wordsToFind = new ArrayList<>();
  wordsToFind.add("Android");
  wordsToFind.add("Apple");
  int count = 0;
  Map<String, Integer> wordsMap = new HashMap<>();
  Pattern p;
  Matcher m;
  for (String s : wordsToFind) {
    p = Pattern.compile(s);
  for (Sentence sentence : sentences) {
   m = p.matcher(sentence.getSentence());
   if (m.find()){
     count++;
    wordsMap.put(s, count);
   }
  }
  }
  Map<String, Integer> sorted = wordsMap
      .entrySet()
      .stream()
      .sorted(comparingByValue())
      .collect(Collectors.toMap(Entry::getKey, Entry::getValue,(e1,e2)->e2,LinkedHashMap::new));
  System.out.println(sorted);
}

public void task_11() throws FileNotFoundException {
  List<Sentence> sentences = bookParser.getSentences2();
  Pattern p = Pattern.compile("h[^t\\s]*t");
  Matcher m;
  System.out.println(sentences);
  for (int i = 0; i < sentences.size(); i++) {
    String sentence = sentences.get(i).getSentence();
    sentence = sentence.replaceAll("h[^t]*t", "");
    sentences.set(i, new Sentence(sentence));
  }
  System.out.println(sentences);
}

public void task_12() throws FileNotFoundException {
  int length = 4;
  List<Word> words = bookParser.getWords2();
  System.out.println(words);
  Iterator<Word> iterator = words.iterator();
  while (iterator.hasNext()){
    Word wordEntity = iterator.next();
    String word = wordEntity.getWord();
    if (word.length() == length){
      char[] chars = word.toCharArray();
      char c = chars[0];
      String s = Character.toString(c);
      if (s.matches("[^aeioyuAEIOYU]")){
        iterator.remove();
      }
    }
  }
  System.out.println(words);
}

public void task_13() throws FileNotFoundException {
  char c = 'a';
  Map<String, Integer> wordMap = new HashMap<>();
  List<Word> wordsEntity = bookParser.getWords2();
  List<String> words = new LinkedList<>();
  for (Word word : wordsEntity) {
    words.add(word.getWord());
  }
  List<String> orderedWords = words.stream().sorted().collect(Collectors.toList());
  for (String orderedWord : orderedWords) {
    int count = 0;
    char[] chars = orderedWord.toCharArray();
    for (int i = 0; i < chars.length; i++) {
      if (chars[i] == c){
        count++;
      }
    }
    wordMap.put(orderedWord, count);
  }
  LinkedHashMap<String, Integer> orderedWordsWithCharNumber = wordMap.entrySet().stream().sorted(comparingByValue())
      .collect(
          Collectors.toMap(Entry::getKey, Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));
  System.out.println(orderedWordsWithCharNumber);
}

public void task_14() throws FileNotFoundException {
  List<Word> wordsEntity = bookParser.getWords2();
  int length = 0;
  String palindrome = "";
  List<String> words = new LinkedList<>();
  for (Word word : wordsEntity) {
    words.add(word.getWord());
  }
  for (int i = 0; i < words.size(); i++) {
    StringBuilder word = new StringBuilder();
    String reverseWord = word.append(words.get(i)).reverse().toString();
    if (words.get(i).equalsIgnoreCase(reverseWord)){
      if (words.get(i).length() > length){
      palindrome = words.get(i);
      length = words.get(i).length();
      }
    }
  }
  System.out.println(palindrome + " length= " + length);
}

  public void task_15() throws FileNotFoundException {
    List<Word> wordsEntity = bookParser.getWords2();
    List<String> words = new LinkedList<>();
    List<String> newWords = new LinkedList<>();
    for (Word word : wordsEntity) {
      words.add(word.getWord());
    }
    for (int i = 0; i < words.size(); i++) {
      String word = words.get(i);
      String firstLetter = word.charAt(0)+"";
      String lastLetter = word.charAt(word.length()-1)+"";
      word = word.replaceAll(firstLetter+"", "");
      word = word.replaceAll(lastLetter, "");
      word = firstLetter + word + lastLetter;
      newWords.add(word);
    }
    System.out.println(newWords);
  }

  public void task_16(int length) throws FileNotFoundException {
    List<Sentence> sentences = bookParser.getSentences2();
    String wordForAppend = "POP";
    for (int i = 0; i < sentences.size(); i++) {
    //  String[] split = sentences.get(i).getSentence().split("[,\\s]");
      String sentence = sentences.get(i).getSentence().replaceAll("\\b\\w{" + length + "}\\b", wordForAppend);
      System.out.println(sentence);
    }

  }



  private int getNumberOfEntries(final String regEx, final String word) {
    final Pattern p = Pattern.compile(regEx);
    final Matcher m = p.matcher(word);
    int counter = 0;
    while (m.find()) {
      counter++;
    }
    return counter;
  }
  private double getVowelsPercents(final String word) {
    final String regEx = "[aeioyuAEIOYU]";
   return getNumberOfEntries(regEx, word) / (double) word.length();
  }

  private int getFirstConsonantLetter(final String word) {
    final String regEx = "[^aeioyuAEIOYU]";
    final Pattern p = Pattern.compile(regEx);
    final Matcher m = p.matcher(word);
    if (m.find()) {
      return (int) m.group()
          .charAt(0);
    }
    return Integer.MAX_VALUE;
  }

}
