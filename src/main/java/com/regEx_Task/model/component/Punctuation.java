package com.regEx_Task.model.component;

import java.util.Objects;

public class Punctuation {
  private String mark;

  public Punctuation() {
  }

  public Punctuation(String mark) {
    this.mark = mark;
  }

  public String getMark() {
    return mark;
  }

  public void setMark(String mark) {
    this.mark = mark;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Punctuation that = (Punctuation) o;
    return Objects.equals(mark, that.mark);
  }

  @Override
  public int hashCode() {
    return Objects.hash(mark);
  }

  @Override
  public String toString() {
    return "Punctuation{" +
        "mark='" + mark + '\'' +
        '}';
  }
}
